{
  description = "A flake for LaTeX projects";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    with flake-utils.lib;
      eachSystem allSystems (system: let
        ## Basename of the master file without extension
        master-file = "sesar";
        pkgs = nixpkgs.legacyPackages.${system};
        tex-pkgs = {
          inherit
            (pkgs.texlive)
            scheme-basic
            latexmk
            fontspec
            mathtools
            polyglossia
            luatexbase
            lualatex-math
            unicode-math
            stmaryrd
            hyperref
            mdwtools
            environ
            algorithmicx
            subfig
            caption
            eqparbox
            sttools
            cite
            pgfplots
            xcolor
            booktabs
            tabulary
            tex-gyre
            tex-gyre-math
            ulem
            ## Your tex packages here
            
            ;
        };
        gyre-math = builtins.elemAt tex-pkgs.tex-gyre-math.pkgs 0;
      in {
        formatter = pkgs.alejandra;

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            glibcLocales
            coreutils
            texlab # LSP server
            gyre-fonts
            # biber ## For bibliography
            (texlive.combine tex-pkgs)
          ];
          shellHook = ''
            export OSFONTDIR="${pkgs.gyre-fonts}/share/fonts//;${gyre-math}/fonts//"
          '';
        };

        packages.default = pkgs.stdenvNoCC.mkDerivation {
          name = "Output";
          src = ./.;
          buildInputs = with pkgs; [
            coreutils
            fontconfig
            gyre-fonts
            (texlive.combine tex-pkgs)
          ];
          phases = ["unpackPhase" "buildPhase" "installPhase"];
          buildPhase = ''
            mkdir -p .cache/texmf-var
            env TEXMFHOME=.cache TEXMFVAR=.cache/texmfvar \
            OSFONTDIR="${pkgs.gyre-fonts}/share/fonts//;${gyre-math}/fonts//" \
            latexmk -interaction=nonstopmode -pdf -lualatex ${master-file}.tex
          '';
          installPhase = ''
            mkdir -p $out
            cp ${master-file}.pdf $out/${master-file}.pdf
          '';
        };
      });
}
