\documentclass[professionalfonts,tikz,aspectratio=169]{beamer}
\usetheme[hideothersubsections]{Hannover}
\usepackage[no-math]{fontspec}
\usepackage{stmaryrd,amsthm,amsmath,amssymb}
\usepackage{arevmath}
%\usepackage{unicode-math}
\usepackage[italic]{mathastext}
\usepackage{mathtools}
\usepackage{color,graphicx,subfig}
\usepackage{tikz,pgfplots}
\usepackage{aircraftshapes}
\usepackage{algorithm,algpseudocode}
\usepackage{tabulary,multirow}
\usepackage{alltt}
\usepackage{booktabs}
\usepackage{pgfpages}
\usepackage{xcolor}

\setmainfont{DejaVu Sans}
%\setmathfont{Tex Gyre DejaVu Math}[version=DejaVu]

\usecolortheme{dolphin}

\setbeameroption{show notes}

\usetikzlibrary{intersections}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{patterns,arrows,positioning}
\usetikzlibrary{external}
\usepgfplotslibrary{statistics}
%\tikzexternalize

\tikzset{%
    %Define standard arrow tip
    >=stealth',
    %Define style for boxes
    punkt/.style={%
           rectangle,
           rounded corners,
           draw=black, very thick,
           text width=6.5em,
           minimum height=2em,
           text centered},
    % Define arrow style
    pildwn/.style={%
           <-,
           thick,
           shorten <=2pt,
           shorten >=2pt,},
    pilup/.style={%
           ->,
           thick,
           shorten <=2pt,
           shorten >=2pt,}
}

\title[Disruption management with reinforcement learning]{%
  Airline disruption management with aircraft swapping and
  reinforcement learning
}
\author[Hondet, Delgado, Gurtner]{G.~Hondet, L.~Delgado, G.~Gurtner}
\institute[ENAC]{École nationale de l'aviation civile}
\date{December 5, 2018}

% Numbering pages ------------------------------------------
\def\swidth{1.6cm}
\setbeamersize{sidebar width left=\swidth}
\setbeamertemplate{sidebar left}
{%
  {\usebeamerfont{title in sidebar}
    \vskip1.5em
    \usebeamercolor[fg]{title in sidebar}
    \insertshorttitle[width=\swidth,center,respectlinebreaks]\par
    \vskip1.25em
  }
  {
    \usebeamercolor[fg]{author in sidebar}
    \usebeamerfont{author in sidebar}
    \insertshortauthor[width=\swidth,center,respectlinebreaks]\par
    \vskip1.25em
  }
  \hbox to2cm{\hss\insertlogo\hss}
  \vskip1.25em
  \insertverticalnavigation{\swidth}
  \vfill
  \hbox to2cm{\hskip0.6cm\usebeamerfont{subsection in
      sidebar}\strut\usebeamercolor[fg]{subsection in
      sidebar}\insertframenumber /\inserttotalframenumber\hfill}
  \vskip3pt
}
% ----------------------------------------------------------

\begin{document}
\begin{frame}[plain]
  \titlepage{}
\end{frame}

\AtBeginSection[]
{%
  \begin{frame}
    \frametitle{Outline}
    \tableofcontents[currentsection]
  \end{frame}
}

\section*{Introduction}

\begin{frame}[c]{Introduction}
  \begin{itemize}
    \item Lower costs due to airline disruptions
    \item Usually, Disruption solution man made by rule of thumb
    \item Aircraft or flight swapping
    \item Reinforcement learning
  \end{itemize}
\end{frame}
\begin{frame}[c]{Current work}
  \begin{itemize}
  \item J.~Clausen \textit{et al.}
    ``Disruption management in the airline industry --- concepts,
    models and methods'', 2009
    \note[item]{analytical methods, graph methods}
  \item R.~S.~Sutton and A.~G.~Barto, \textit{Reinforcement Learning:
      An Introduction}, 2017
  \item V.~Mnih \textit{et al.}, ``Playing Atari with deep
    reinforcement learning'', 2013
  % \item A.~Cook and G.~Tanner, ``European airline delay cost reference
  %   values'', 2005
  \end{itemize}
  \begin{block}{Work done here}
    Machine learning technique to discover interesting swap combinations
  \end{block}
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Simulator}
\subsection{Specification}
\begin{frame}[c]{Specification of simulator}
  \begin{block}{Purpose}
    \begin{itemize}
    \item Evaluate the delay on a fleet, on a day of operation
    \item estimate generated costs
    \item perform actions on the fleet
    \end{itemize}
  \end{block}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{exampleblock}{Does}
        \begin{itemize}
        \item model reactionary delay
          \note[item]{Reactionary delay: can be influenced by airline
            operations, 44\% of total delay}
        \item include other delays as probability distributions
          \note[item]{taxiing, atfm, miscellaneous}
        \item simulate aircraft swapping and its consequences
        \end{itemize}
      \end{exampleblock}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{alertblock}{Does not}
        \begin{itemize}
        \item model crew management, nor passengers flow
          \note[item]{not taking into account itineraries}
          \note[item]{not waiting for other flights}
        \item manage stand-by aircraft,
        \item modify or cancel legs
        \end{itemize}
      \end{alertblock}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Mechanisms}
\begin{frame}{Mechanisms}
  \begin{block}{Timestep}
    \( \forall i \in \llbracket 1, m \rrbracket, t_i \) is the time of
    the \( i \)\textsuperscript{th} landing of the day
    \begin{center}
      \( (t_1, t_2, \dots, t_m) \)
    \end{center}
  \end{block}
  \begin{block}{Actions}
    Allow to alter the simulation,\\
    \begin{center}
      ``swap with aircraft \( a \)''
    \end{center}
    \note[item]{swappable aircraft list computed at each time step}
    \note[item]{if aircraft has the current airport as departure
      airport in its remaining flight path}
  \end{block}
  \begin{block}{Cost}
    Immediate cost of a swap
    \begin{center}
      ``swapping with \( a \) costs \( c \)''
    \end{center}
  \end{block}
\end{frame}

\subsection{Cost \& calibration}
\begin{frame}{Cost \& calibration}
  \begin{block}{Cost of what}
    Delay at departure of the flight after swap
  \end{block}
  \begin{block}{Characteristics}
    \begin{itemize}
    \item non linear
    \item increasing derivative
      \[ c(d_1 + d_2) > c(d_1) + c(d_2) \]
      \note[item]{increasing derivative e.g.\ parabolic}
    \item depends on the aircraft type
    \end{itemize}
  \end{block}
  \begin{block}{Calibration}
    Calibrated against Eurocontrol ``Coda Digest 2017''
    \note[item]{Reactionary delay distribution}
    \note[item]{Departure delay distribution}
    \note[item]{Proportion of delayed flights}
  \end{block}
\end{frame}

\section{Q learning as solver}
\subsection{Principle and method}
\begin{frame}[c]{Principle}
  \begin{block}{Reinforcement learning}
    \begin{itemize}
    \item Interaction between an agent and its environment
    \item Find a policy \( \pi \colon \text{state} \to \text{action} \)
    \end{itemize}
  \end{block}
  \begin{figure}[!t]
    \begin{center}
      \begin{tikzpicture}[node distance=1cm, auto,]
        %nodes
        \node[punkt] (env) {Environment};
        \node[punkt, inner sep=5pt,below=1.2cm of env]
        (agent) {Agent}
        edge[pilup,bend right=60] node[right] {Action} (env.east)
        edge[pildwn,bend left=70] node[left] {New state} (env.west)
        edge[pildwn,bend left=50] node[right] {Reward} (env.west);
      \end{tikzpicture}
    \end{center}
    \caption{Reinforcement learning principle}\label{fig:rlp}
  \end{figure}
\end{frame}
\begin{frame}{Theoretical basis}
  State \( s \in \mathcal{S} \), action \( a \in \mathcal{A} \).
  \begin{block}{Maximised value}
    \begin{equation}
      \mathbb{E} \left( \sum_{t = 0}^{T_f} r_t \right)
      \longleftrightarrow Q(s, a)
    \end{equation}
    \note[item]{\( T_f \) end of life / episode}
  \end{block}
  \note{
    \( Q(s, a) \sim \) expectation of final accumulated reward knowing
    the agent has applied action \( a \) on state \( s \)
  }
  \begin{block}{Bellman equation}
    \begin{equation}
      % V^{*}_{\pi}(s) = \max_{a \in \mathcal{A}} \left\{
      % r(s, a) + \sum_{s' \in \mathcal{S}} p\left(s' \middle| s,
      % a\right) V^{*}_{\pi}(s')
      % \right\}
      Q^{*}(s, a) = r(s, a) + \sum_{s' \in \mathcal{S}} p(s' | s, a)
      \max_{a'}Q^{*}(s', a')
    \end{equation}
    \note[item]{Linear system with \( |\mathcal{S}| \) unknowns, too big}
  \end{block}
  \begin{itemize}
  \item Dynamic programming
  \item Monte Carlo simulations
  \end{itemize}
\end{frame}
\subsection[Q learning]{Q learning algorithm and implementation}
\begin{frame}{Q learning algorithm}
  \begin{algorithmic}
    \Procedure{Q-learning}{$Q$}
    \State{}\( s \gets \) initial state
    \While{episode not finished}
    \State{}\( a \gets \) choose an action from a set
    \State{}play \( a \), observe reward \( r \) and new state \( s'
    \)
    \State{}\( Q \gets \) update \( Q \) with \( (s, a, r, s') \)
    \State{}\( s \gets s' \)
    \EndWhile{}
    \EndProcedure{}
  \end{algorithmic}
\end{frame}
\begin{frame}{Lookup table implementation}
  \[
  Q =
  \begin{bmatrix}
    Q(s_{\textcolor{blue}{0}}, a_{\textcolor{orange}{0}}) &
    Q(s_0, a_{\textcolor{orange}{1}}) &
    \cdots & \phantom{Q(s_0, a_{n-1})} &
    \phantom{Q(s_0, a_n)} \\
    Q(s_{\textcolor{blue}{1}}, a_0) &
    Q(s_1, a_1) & \cdots & & \\
    \vdots & \vdots & & & \\
    & & Q(s, a) & & \\
    & & & & \\
    & & & &
  \end{bmatrix}
  \]
  \begin{block}{Update formula}
    State \( s \), action \( a \), reward \( r \) and next state \( s' \).
    \begin{equation}
      Q(s, a) \leftarrow Q(s, a) + \alpha\left(
      r + \max_{a'} Q(s', a') - Q(s, a)
      \right)
    \end{equation}
    \note{$\alpha$ learning rate \( \in [0, 1] \)}
  \end{block}
\end{frame}
\begin{frame}{Choosing an action}
  \note{exploitation exploration}
  \begin{block}{Bandit methods}
    Maximise reward, minimise regret
  \end{block}
  \begin{exampleblock}{Upper confidence bound}
    \begin{equation}
      \underbrace{Q_t(s, a)}_{\text{exploitation}} + \pause{}
      c \underbrace{\sqrt{\frac{\ln t}{N_t(s, a)}}}_{\text{exploration}}
    \end{equation}
  \end{exampleblock}
\end{frame}

\subsection[Practical training]{Practical training with the simulator}
\begin{frame}[c]{Final algorithm}
  \begin{algorithmic}
    \Procedure{Q-learning}{$Q, c, \alpha, \mathcal{A}$}
    \State{}\( s \gets \) initial state
    \While{episode not finished}
    \State{}\( a \gets \) \Call{ChooseAction}{$\mathcal{A}, c$}
    \State{}\( (r, s') \gets \)
    \Call{SimulationStep}{$s, a$}
    \State{}\( Q(s, a) \gets Q(s, a) + \alpha \left[r_t + \max_{a' \in
      \mathcal{A}} Q(s', a') - Q(s, a)\right] \)
    \State{}\( s \gets s' \)
    \EndWhile{}
    \EndProcedure{}
  \end{algorithmic}
  % \begin{figure}[!t]
  %   \begin{center}
  %     \begin{tikzpicture}[node distance=1cm, auto,]
  %       %nodes
  %       \node[punkt] (env) {Environment}
  %       edge[pilup,loop above] node[above] {Simulation step} (env.north);
  %       \node[punkt, inner sep=5pt,below=1.2cm of env]
  %       (agent) {Agent}
  %       edge[pilup,loop below] node[below] {Q matrix update} (agent.south)
  %       edge[pilup,bend right=60] node[right] {\( a \)} (env.east)
  %       edge[pildwn,bend left=70] node[left] {\( (s_{t + 1},
  %         \mathcal{A}_{t + 1}) \)} (env.west)
  %       edge[pildwn,bend left=50] node[right] {\( r_t \)} (env.west);
  %     \end{tikzpicture}
  %   \end{center}
  %   \caption{Reinforcement learning principle}\label{fig:rlp-more}
  % \end{figure}
  \note{Action set depends on the state}
\end{frame}
\begin{frame}{Implementing the training}
  \begin{block}{Hyperparameters}
    \begin{itemize}
    \item Exploitation exploration trade off
    \item Initial \( Q \) value
      \note[item]{higher q \( \rightarrow \) exploration}
    \end{itemize}
  \end{block}
  \begin{block}{Learning rate}
    \begin{columns}
      \begin{column}{0.5\textwidth}
        \begin{equation}
          \sum_{n \geq 0} \alpha_n = \infty; \quad \sum_{n \geq 0}
          \alpha_n^2 \in \mathbb{R}
        \end{equation}
      \end{column}
      \begin{column}{0.5\textwidth}
        % \( N_t(s, a) = \) number of visits of cell \( (s, a) \).
        \begin{equation}
          \alpha_n = \frac{1}{N_{t}(s, a)}
        \end{equation}
        \note[item]{Learning rate used at each update}
      \end{column}
    \end{columns}
  \end{block}
  \begin{block}{Chaining training sessions}
    \begin{equation}
      Q^1
      \xrightarrow{\text{training}} Q^2
      \xrightarrow{\text{training}} \cdots
      \xrightarrow{\text{training}} Q^{*}
    \end{equation}
    \note[item]{each episode gives an updated matrix}
  \end{block}
  % \begin{exampleblock}{Learning rate}
  %   \note[item]{Learning rate used at each update}
  %   \( N_t(s, a) = \) number of visits of cell \( (s, a) \).
  %   \begin{equation}
  %     \alpha_n = \frac{1}{N_{t}(s, a)}
  %   \end{equation}
  % \end{exampleblock}
\end{frame}
\begin{frame}{State space}
  \begin{alertblock}{Observation}
    Partial information of the environment,
    \( \mathcal{O} \) the set of observations,
    \[ (\mathcal{S}, \mathcal{A}) \xrightarrow{\phi} (\mathcal{O}, \mathcal{A})
    \xrightarrow{Q} \mathbb{R} \]
  \end{alertblock}
  \begin{block}{Choice of \( \phi \)}
    \begin{itemize}
    \item Carries enough information
      \note[item]{enough information to know what is the best action}
    \item But not too specific
      \note[item]{not too specific to ensure states are visited
        enough}
    \item Time independent
    \end{itemize}
  \end{block}
\end{frame}

\section{Experiments and results}
\subsection{Experimental setup}
\begin{frame}{Experimental setup}
  \begin{itemize}
  \item Schedule: Vueling, October 12, 2014
  \item 6 aircraft, 14 stations, 35 flights
    \note[item]{Each aircraft has at least one swap possibility}
  \end{itemize}
  \begin{block}{Observation}
    Two different observations tested.
    \note[item]{quickly give overview and differences of observations}
  \end{block}
  \begin{block}{Disruption}
    Artificial delay added.
    \note[item]{delay of 3h for compensation}
  \end{block}
  \begin{block}{Hyperparameters}
    \[ (p_d, c, q_i) = (0.06, 10, -90000) \]
  \end{block}
\end{frame}
\begin{frame}{Output format}
  Spreadsheet like \texttt{parquet} files
  \begin{block}{Columns}
    \begin{columns}
      \begin{column}{0.5\textwidth}
        \begin{itemize}
        \item delays
          \begin{itemize}
          \item atfm delay
          \item departure delay
          \item miscellaneous delays
          \item reactionary delay
          \item artificial delay added
          \item taxi time
          \end{itemize}
        \item action and reward
          \begin{itemize}
          \item action number
          \item swap or not
          \item cost
          \item cumulative reward
          \end{itemize}
        \end{itemize}
      \end{column}
      \begin{column}{0.5\textwidth}
        \begin{itemize}
        \item simulation information
          \begin{itemize}
          \item departure destination
          \item departure origin
          \item leg duration
          \item departure sobt
          \item tail number
          \item tail number of swapped aircraft
          \item time in the simulation
          \end{itemize}
        \item Q learning data
          \begin{itemize}
          \item state-action couple visit count
          \item \( Q \) value
          \end{itemize}
        \end{itemize}
      \end{column}
    \end{columns}
  \end{block}

\end{frame}

\subsection{Results}
\begin{frame}{Learning process}
  \begin{figure}[t]
    \centering
    \tikzsetnextfilename{deepmind_metric}
    \begin{tikzpicture}
      \begin{axis}[%
        line width=0.05,
        mark size=0.1,
        xlabel=episode,
        ylabel=\(\mathbb{E}\left(\sum_t \gamma^t r_t\right)\),
        legend pos = south east,
        height=6.5cm,
        %width=8cm
        ]
        \addplot+ table[x expr = {\lineno}, y index = 0] {./data/5kp_06_dqn.csv};
        \addplot+ table [x expr = {\lineno}, y index = 0] {./data/ns5kp06_dqn.csv};
        \legend{obs.1 ,obs.2}
      \end{axis}
    \end{tikzpicture}
    \caption{Average maximum Q values over 5000
      episodes.}\label{fig:avg-maxq-5kp0.06}
  \end{figure}
  \note{Metric from google deepmind, average Q value on a defined set
    of states}
\end{frame}
\begin{frame}{Comparing with idle behaviour}
  \begin{figure}[t]
    \begin{tikzpicture}
      \begin{axis}[
        xlabel = reward,
        ytick={1,2,3},
        yticklabels={ag.\ 1,idle,ag.\ 2},
        height=6.5cm,
        width=8cm
        ]
        % First observation
        \addplot+[boxplot] table[col sep=comma, y index=1] {./data/cmp20kp06.csv};

        % Idle
        \addplot+[boxplot] table[col sep=comma, y index=2] {./data/cmpns20kp06.csv};

        % Second observation
        \addplot+[boxplot] table[col sep=comma, y index=1] {./data/cmpns20kp06.csv};
      \end{axis}
    \end{tikzpicture}
    \caption{Comparing the idle behaviour with the agent.}\label{fig:cmp-agent-idle}
  \end{figure}
  \note[item]{8 swaps in average, very high, not enough exploration?}
  \note[item]{some states only visited a dozen times even after 20,000
    episodes}
\end{frame}
\section*{Conclusion}
\begin{frame}[c]{Conclusion}
  \begin{block}{Results}
    Cost reduced in some conditions, not reliable enough.
    Potential lines of research.
  \end{block}
  \begin{block}{Perspectives}
    \begin{itemize}
    \item refine observations
      \note[item]{better variables (fisher's discriminant, PCA,
        variance scoring, autoencoding network, \&c.)}
    \item more sophisticated reinforcement learning techniques
      \note[item]{Neural network, A3C}
    \item develop further the simulator
      \note[item]{more actions, like cancelling flights, speeding up
        flight \&c.}
      \note[item]{could ease the work of the algorithm, having more freedom}
    \end{itemize}
  \end{block}
\end{frame}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
